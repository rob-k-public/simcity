package org.rob.simcity.web.dto;

public class BuildingDto {

    private int x;
    private int y;
    private String abbreviation;

    public BuildingDto(int x, int y, String abbreviation) {
        this.x = x;
        this.y = y;
        this.abbreviation = abbreviation;
    }

    public BuildingDto() {
        //default constructor
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
}
