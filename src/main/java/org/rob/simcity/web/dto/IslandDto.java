package org.rob.simcity.web.dto;

import java.util.List;

public class IslandDto {

    private List<BuildingDto> buildings;

    public IslandDto(List<BuildingDto> buildings) {
        this.buildings = buildings;
    }

    public IslandDto() {
        //default constructor
    }

    public List<BuildingDto> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<BuildingDto> buildings) {
        this.buildings = buildings;
    }
}
