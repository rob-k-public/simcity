package org.rob.simcity.web.facade;

import org.rob.simcity.business.building.service.BuildingService;
import org.rob.simcity.web.dto.IslandDto;
import org.springframework.stereotype.Component;

@Component
public class IslandFacadeImpl implements IslandFacade {

    private final BuildingService buildingService;
    private final IslandMapper islandMapper;

    public IslandFacadeImpl(BuildingService buildingService, IslandMapper islandMapper) {
        this.buildingService = buildingService;
        this.islandMapper = islandMapper;
    }

    @Override
    public IslandDto createIsland(int width, int height) {
        return islandMapper.map(buildingService.createIsland(width, height));
    }

    @Override
    public IslandDto getIsland() {
        return islandMapper.map(buildingService.getIsland());
    }
}
