package org.rob.simcity.web.facade;

import org.rob.simcity.business.building.entity.BuildingEntity;
import org.rob.simcity.business.building.entity.Coordinates;
import org.rob.simcity.web.dto.BuildingDto;
import org.rob.simcity.web.dto.IslandDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class IslandMapper {

    public IslandDto map(Map<Coordinates, BuildingEntity>island){
        List<BuildingDto> buildings = island.entrySet().stream()
                .map(entry -> mapBuilding(entry.getValue(), entry.getKey()))
                .collect(Collectors.toList());

        return new IslandDto(buildings);
    }

    private BuildingDto mapBuilding(BuildingEntity entity, Coordinates coordinates){
        return new BuildingDto(coordinates.getX(), coordinates.getY(), entity.getAbbreviation());
    }
}
