package org.rob.simcity.web.facade;

import org.rob.simcity.web.dto.IslandDto;

public interface IslandFacade {

    IslandDto createIsland(int width, int height);

    IslandDto getIsland();
}
