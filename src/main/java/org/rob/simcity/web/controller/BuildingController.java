package org.rob.simcity.web.controller;

import org.rob.simcity.web.dto.IslandDto;
import org.rob.simcity.web.facade.IslandFacade;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/buildings")
public class BuildingController {

    private final IslandFacade islandFacade;

    public BuildingController(IslandFacade islandFacade) {
        this.islandFacade = islandFacade;
    }

    @PutMapping("/_create")
    public IslandDto createIsland(@RequestParam(value = "width", defaultValue = "5")int width,
                                  @RequestParam(value = "height", defaultValue = "5")int height){
        return islandFacade.createIsland(width, height);
    }

    @GetMapping
    public IslandDto getIsland(){
        return islandFacade.getIsland();
    }
}
