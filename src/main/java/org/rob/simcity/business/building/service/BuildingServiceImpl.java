package org.rob.simcity.business.building.service;

import org.rob.simcity.business.building.entity.BuildingEntity;
import org.rob.simcity.business.building.entity.Coordinates;
import org.rob.simcity.dao.island.BuildingRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Service
public class BuildingServiceImpl implements BuildingService {

    private final BuildingRepository buildingRepository;

    public BuildingServiceImpl(BuildingRepository buildingRepository) {
        this.buildingRepository = buildingRepository;
    }

    @Override
    public Map<Coordinates, BuildingEntity> createIsland(int width, int height) {
        IntStream.rangeClosed(0, width)
                .forEach(w -> IntStream.rangeClosed(0, height)
                        .forEach(h -> {
                            Coordinates coordinates = Coordinates.of(w, h);
                            buildingRepository.save(coordinates, BuildingEntity.empty(coordinates));
                        }));
        return buildingRepository.loadIsland();
    }

    @Override
    public void build(List<Coordinates> coordinates, BuildingEntity building) {
        if (allSpacesAvailable(coordinates)) {
            coordinates.forEach(c -> buildingRepository.save(c, building));
        }
    }

    @Override
    public void demolish(List<Coordinates> coordinates) {
        coordinates.forEach(c -> buildingRepository.save(c, BuildingEntity.empty(c)));
    }

    @Override
    public Map<Coordinates, BuildingEntity> getIsland() {
        return buildingRepository.loadIsland();
    }

    private boolean allSpacesAvailable(List<Coordinates> coordinates) {
        return coordinates.stream()
                .map(buildingRepository::loadBuilding)
                .allMatch(BuildingEntity::isBuildable);
    }
}
