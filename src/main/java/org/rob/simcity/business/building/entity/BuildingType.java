package org.rob.simcity.business.building.entity;

public enum BuildingType {
    EMPTY(1, "EMPTY", 0d, 0d),
    BEACH_WITH_SUNBEDS(1, "C", 5000d, 100d),
    FOREST_OF_PALM_TREES(1, "T", 5000d, 500d),
    BAR(1, "B", 1000d, 2000d),
    RESTAURANT(1, "R", 12000d, 2500d),
    SHOP(2, "S", 15000d, 3000d),
    EXCURSTION_CENTER(2, "E", 20000d, 5000d),
    TOURIST_OFFICE(2, "I", 80000d, 10000d),
    HOTEL(4, "H", 2000000d, 500000d),
    HARBOR(6, "Ha", 5000000d, 1000000d),
    AIRPORT(6, "A", 6000000d, 1000000d);

    private final int numberOfSquares;
    private final String abbreviation;
    private final Double price;
    private final Double upkeep;

    BuildingType(int numberOfSquares, String abbreviation, Double price, Double upkeep) {
        this.numberOfSquares = numberOfSquares;
        this.abbreviation = abbreviation;
        this.price = price;
        this.upkeep = upkeep;
    }

    public int getNumberOfSquares() {
        return numberOfSquares;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public Double getPrice() {
        return price;
    }

    public Double getUpkeep() {
        return upkeep;
    }
}
