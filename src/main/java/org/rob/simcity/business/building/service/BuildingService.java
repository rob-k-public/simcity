package org.rob.simcity.business.building.service;

import org.rob.simcity.business.building.entity.BuildingEntity;
import org.rob.simcity.business.building.entity.Coordinates;

import java.util.List;
import java.util.Map;

public interface BuildingService {

    Map<Coordinates, BuildingEntity> createIsland(int width, int height);

    void build(List<Coordinates> coordinates, BuildingEntity building);

    void demolish(List<Coordinates> coordinates);

    Map<Coordinates, BuildingEntity> getIsland();
}
