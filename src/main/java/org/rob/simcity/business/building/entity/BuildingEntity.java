package org.rob.simcity.business.building.entity;

import java.util.Collections;
import java.util.List;

public class BuildingEntity {

    private List<Coordinates> coordinates;
    private int numberOfSquaresNeeded;
    private String abbreviation;
    private Double price;
    private Double upkeep;
    private BuildingType type;

    private BuildingEntity(List<Coordinates> coordinates, BuildingType type) {
        this.coordinates = coordinates;
        this.type = type;
        numberOfSquaresNeeded = type.getNumberOfSquares();
        abbreviation = type.getAbbreviation();
        price = type.getPrice();
        upkeep = type.getUpkeep();
    }

    public static BuildingEntity of(List<Coordinates> coordinates, BuildingType type) {
        return new BuildingEntity(coordinates, type);
    }

    public static BuildingEntity empty(Coordinates coordinates){
        return new BuildingEntity(Collections.singletonList(coordinates), BuildingType.EMPTY);
    }

    public boolean isBuildable(){
        return type == BuildingType.EMPTY;
    }

    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    public int getNumberOfSquaresNeeded() {
        return numberOfSquaresNeeded;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public Double getPrice() {
        return price;
    }

    public Double getUpkeep() {
        return upkeep;
    }
}
