package org.rob.simcity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimcityApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimcityApplication.class, args);
    }
}
