package org.rob.simcity.dao.island;

import org.rob.simcity.business.building.entity.BuildingEntity;
import org.rob.simcity.business.building.entity.Coordinates;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class BuildingRepositoryImpl implements BuildingRepository {

    private Map<Coordinates, BuildingEntity> island;

    public BuildingRepositoryImpl() {
        island = new HashMap<>();
    }

    @Override
    public void save(Coordinates coordinates, BuildingEntity building) {
        island.put(coordinates, building);
    }

    @Override
    public Map<Coordinates, BuildingEntity> loadIsland() {
        return island;
    }

    @Override
    public BuildingEntity loadBuilding(Coordinates coordinates) {
        return island.get(coordinates);
    }
}
