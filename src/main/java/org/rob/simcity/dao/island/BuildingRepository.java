package org.rob.simcity.dao.island;

import org.rob.simcity.business.building.entity.BuildingEntity;
import org.rob.simcity.business.building.entity.Coordinates;

import java.util.Map;

public interface BuildingRepository {

    void save(Coordinates coordinates, BuildingEntity building);

    Map<Coordinates, BuildingEntity> loadIsland();

    BuildingEntity loadBuilding(Coordinates coordinates);
}
